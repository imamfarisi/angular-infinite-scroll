import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  
  data : number[] = []

  constructor() {
    this.addData()
  }

  onScroll() : void {
    this.addData()
  }

  addData() : void {
    for(let i=1; i<=100; i++){
      this.data.push(i)
    }
  }
}
